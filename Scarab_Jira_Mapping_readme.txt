====

    Scarab to JIRA Migration Tool

    This software is copyright (c) 2012 ELAXY Busniess & Service GmbH & Co KG and is released
    under the terms of CDDL v 1.0 - see cddlv1.0.txt.

    Feel free to use or modifiy this software under the terms of CDDL v 1.0. However, if you
    make any modifications, any feedback to the author is appriciated.

    As this is not a commercial project, only limited support is available under

    opensource@elaxy.de

    Version 1.0: 01. July 2012 - Initial public release
====

Prioritaeten Mapping Scarab -> JIRA:
-----------------------------------
Scarab:         JIRA: (select * from priority;)              
Blocker         Blocker       ( id: 1 )
Hoch            Kritisch      ( id: 2 )
                Major         ( id: 3 )                                
Normal          Normal        ( id: 6 )
Niedrig         Minor         ( id: 4 )
                Trivial       ( id: 5 )

Status Mapping Scarab -> JIRA 'Status':
---------------------------------------
Scarab:                 JIRA:
neu                     Open          ( id: 1 )
zugewiesen              In Progress   ( id: 3 )
gefixt                  Resolved      ( id: 5 )
wiedereröffnet          Reopened      ( id: 4 )
geschlossen             Closed        ( id: 6 )


Ergebnis Mapping Scarab -> JIRA 'Loesung':
------------------------------------------
Scarab:                 JIRA:
Behoben                 Fixed             (Behoben)                         ( id: 1 )
Duplikat                Duplicate         (Duplikat)                        ( id: 3 )
Ungültig                Won´t fixed       (Wird nicht korrigiert)           ( id: 2 )
                        Incomplete        (Unvollständig)                   ( id: 4 )
Nicht Nachvollziehbar   Cannot Reproduce  (Kann nicht reproduziert werden)  ( id: 5 )


Typ(Scarab) Mapping Scarab -> JIRA(Vorgangstyp):
------------------------------------------
Ticket
 Tickettyp:
 bug                          Bug             ( id: 1 )
 change                       Neue Funktion   ( id: 2 )
 refactoring                  Aufgabe         ( id: 3 )
 todo                         Aufgabe         ( id: 3 )
Bug                           Bug             ( id: 1 )
Change                        Neue Funktion   ( id: 2 )
Requirement                   Neue Funktion   ( id: 2 )
Task                          Aufgabe         ( id: 3 )


