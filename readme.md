# Scarab2Jira Migration Tool

---

## What is it?


It is a java tool for converting projects and tickets from the issue tracking system [Scarab](http://scarab.tigris.org) to the issue tracking system [JIRA](http://www.atlassian.com/jira).

## System Requirements
- tested on WinOS, but should run also on other operating systems
- JDK 1.6
- Maven 3  
- tested with Jira 4.x


## Overview

The following graphic illustrates the components and how they work together:

![Architecture](https://bitbucket.org/elaxy/scarab-to-jira-migration-tool/raw/master/scarab2jira.png)


## Scarab2Jira Tool Usage

    ELAXY Scarab 2 Jira Migration Tool
    Release 1.0.0
    Copyright (c) 2010 ELAXY Business Solution & Services GmbH & Co. KG.
    ERROR: invalid commandline - Missing required options: l, i
    USAGE: -i <parse scarab export file> [-j <start jira import>] -l <log4j filename>
    OPTIONEN:
       -i <parse scarab export file>   scarab xml export
       -j <start jira import>          property file
       -l <log4j filename>             configurationfile for Logging (Log4j)
    RETURNVALUES:
        0                 Erfolgreiche Verarbeitung
        1                 Fehler in Konfigurationsdatei oder Kommandozeile
        2                 Systemfehler bei der Verarbeitung

Omitting the parameter '-j' can be used to validate the scarab xml file.

## How to Migrate?
 
It is recommended to execute the migration within eclipse ide.

* configure your jira server to **Accept remote API calls** ([Configuring JIRA Options](https://confluence.atlassian.com/display/JIRA/Configuring+JIRA+Options): "Controls whether to allow remote client access (via XML-RPC or SOAP) to this JIRA installation, for authenticated users. Default: OFF") 

* Modify **pom.xml** for your specific environment. The property **jiraurl** must contain the url to the wsdl of your jira server instance. The jira server must be up & running during your maven compile. A [wsdl2java](http://mojo.codehaus.org/axistools-maven-plugin/wsdl2java-mojo.html) build step read the wsdl from your jira server and generates java stub code.

* Run **mvn clean verify** to generate stubs for using JIRA webservices.

With the additional maven profile parameter you can select the related environment

    mvn -Ptest clean verify
    mvn -Pproduction clean verify

* Export the scarab tickets, from the project which you want to migrate, as xml and put them in a subdirectory of the input folder.

* The scarab export files must be modified manually before further processing with scarab2jira tool. Delete manually the second line 
*<!DOCTYPE scarab-issues SYSTEM "http://scarab.tigris.org/dtd/scarab-0.21.0.dtd">* from the scarab-issues-xxx.xml file.
Otherwise this error occurs (if you work behind a proxy without setting the proxy settings in the environment):

    ELAXY Scarab 2 Jira Migration Tool
    Release 1.0.0
    Copyright (c) 2010 ELAXY Business Solution & Services GmbH & Co. KG.
    
    dd-mm-yyyy hh:mm:ss.msec | ERROR | Scarab2Jira               | file i/o error Connection timed out: connect"

* known bug in scarab export: if scarab ticket contains urls with more than one '&' the other '&' chars will not be correctly masked. solution: replace all '&' chars manually with '&amp'

* Copy all scarab attachments into a local folder.

* Create a new folder under *setup/configuration* where you put a configured *scarab2jira.properties* file.

* Configure the 6 mapping csv files under *setup/configuration/common* for your needs.

* Run the application with the following options: **-j setup/configuration/projectname/jira.properties -i input/projectname/issues.xml -l setup/configuration/log4j.xml** 
   


### additional hints
- historical scarab ticket change entries will be converted to a single jira comment entry
- scarab links to other scarab projects will not be converted to related links in jira
- gaps in scarab ticket enummerations will be filled up on jira side with dummy tickets to keep the identifier numbers
- the mapping values of the configuration files mappingJiraPriority2JiraPriorityId.csv, mappingJiraSolution2JiraSolutionId.csv, mappingJiraType2JiraTypeId.csv, mappingPriority.csv, mappingSolution.csv, mappingType.csv should be checked against the values in the jira database

### success story
We have converted 10000+ tickets from different structured scarab project successfully to jira.

## License
COMMON DEVELOPMENT AND DISTRIBUTION LICENSE (CDDL) Version 1.0

## Contact
opensource@elaxy.de

