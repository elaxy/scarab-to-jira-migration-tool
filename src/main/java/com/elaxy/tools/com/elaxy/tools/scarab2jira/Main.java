/**
 *
 * Scarab to JIRA Migration Tool
 *
 * This software is copyright (c) 2012 ELAXY Busniess & Service GmbH & Co KG and is released
 * under the terms of CDDL v 1.0 - see cddlv1.0.txt.
 *
 * Feel free to use or modifiy this software under the terms of CDDL v 1.0. However, if you
 * make any modifications, any feedback to the author is appriciated.
 *
 * As this is not a commercial project, only limited support is available under
 *
 * opensource@elaxy.de
 *
 * Version 1.0: 01. July 2012 - Initial public release
 */

package com.elaxy.tools.com.elaxy.tools.scarab2jira;

import java.io.File;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.xml.DOMConfigurator;

public final class Main
{
  public static final String PROGRAM_VERSION           = "1.0.0";

  private static final int   STATUS_SUCCESS            = 0;
  private static final int   STATUS_ERROR_COMMAND_LINE = 1;
  private static final int   STATUS_ERROR_SYSTEM       = 2;

  @SuppressWarnings( "static-access" )
  public static void main( String[] args )
  {
    System.out.println( "ELAXY Scarab 2 Jira Migration Tool" );
    System.out.println( "Release " + Main.PROGRAM_VERSION );
    System.out.println( "Copyright (c) 2010 ELAXY Business Solution & Services GmbH & Co. KG." );
    System.out.println();

    Options options = new Options();
    options.addOption( OptionBuilder.hasArgs( 1 ).isRequired( true ).withArgName( "log4j filename" ).withDescription(
        "configurationfile for Logging (Log4j)" ).create( 'l' ) );
    options.addOption( OptionBuilder.hasArgs( 1 ).isRequired( true ).withArgName( "parse scarab export file" ).withDescription(
        "scarab xml export" ).create( 'i' ) );
    options.addOption( OptionBuilder.hasArgs( 1 ).isRequired( false ).withArgName( "start jira import" ).withDescription( "property file" ).create(
        'j' ) );    

    try
    {
      CommandLine cmd = new GnuParser().parse( options, args, false );

      Main.configureLogging( cmd.getOptionValue( 'l' ) );

      Scarab2Jira scarab2Jira = new Scarab2Jira();

      if ( scarab2Jira.parseScarabXmlfile( cmd.getOptionValue( 'i' ) ) )
      {        
        if ( cmd.hasOption( 'j' ) )
        {
          scarab2Jira.checkIfAttachementsAreAvailable( cmd.getOptionValue( 'j' ) );
          
          if ( scarab2Jira.importToJira( cmd.getOptionValue( 'j' ) ) )
          {
            System.exit( Main.STATUS_SUCCESS );
          }
          else
          {
            System.exit( Main.STATUS_ERROR_SYSTEM );
          }
        }  
      }
      else
      {
        System.exit( Main.STATUS_ERROR_SYSTEM );
      }
    }
    catch ( ParseException e )
    {
      PrintWriter writer = new PrintWriter( new OutputStreamWriter( System.out, Charset.forName("ISO-8859-1") ), true );
      HelpFormatter helpFormatter = new HelpFormatter();
      helpFormatter.setSyntaxPrefix( "USAGE:" );

      System.out.println( "ERROR: invalid commandline - " + e.getMessage() );
      System.out.println();
      helpFormatter.printUsage( writer, 100, "", options );
      System.out.println( "\nOPTIONEN:" );
      helpFormatter.printOptions( writer, 100, options, 3, 3 );
      System.out.println( "\nRETURNVALUES:" );
      System.out.printf( "   %1$2d                 Erfolgreiche Verarbeitung%n", Integer.valueOf( Main.STATUS_SUCCESS ) );
      System.out.printf( "   %1$2d                 Fehler in Konfigurationsdatei oder Kommandozeile%n",
          Integer.valueOf( Main.STATUS_ERROR_COMMAND_LINE ) );
      System.out.printf( "   %1$2d                 Systemfehler bei der Verarbeitung%n", Integer.valueOf( Main.STATUS_ERROR_SYSTEM ) );

      System.out.println();

      System.exit( Main.STATUS_ERROR_COMMAND_LINE );
    }
    catch ( SystemException e )
    {
      e.printStackTrace();
      System.exit( Main.STATUS_ERROR_SYSTEM );
    }
    catch ( Throwable e )
    {
      e.printStackTrace();
      System.exit( Main.STATUS_ERROR_SYSTEM );
    }
  }

  private static void configureLogging( String filename ) throws SystemException
  {
    File loggingConfigurationFile = new File( filename );

    if ( loggingConfigurationFile.exists() )
    {
      if ( loggingConfigurationFile.isFile() )
      {
        if ( loggingConfigurationFile.canRead() )
        {
          if ( loggingConfigurationFile.getAbsolutePath().endsWith( ".xml" ) )
          {
            DOMConfigurator.configure( loggingConfigurationFile.getAbsolutePath() );
          }
          else
          {
            PropertyConfigurator.configure( loggingConfigurationFile.getAbsolutePath() );
          }
        }
        else
        {
          throw new SystemException( "Die angegebene Log4J Konfigurationsdatei \"" + loggingConfigurationFile.getAbsolutePath()
              + "\" kann nicht gelesen werden." );
        }
      }
      else
      {
        throw new SystemException( "Die angegebene Log4J Konfigurationsdatei \"" + loggingConfigurationFile.getAbsolutePath()
            + "\" bezieht sich auf ein Verzeichnis und nicht auf eine Datei." );
      }
    }
    else
    {
      throw new SystemException( "Die angegebene Log4J Konfigurationsdatei \"" + loggingConfigurationFile.getAbsolutePath()
          + "\" existiert nicht." );
    }
  }

  private Main()
  {
    // Hide Utility Class Constructor
  }
}
