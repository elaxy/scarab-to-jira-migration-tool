/**
 *
 * Scarab to JIRA Migration Tool
 *
 * This software is copyright (c) 2012 ELAXY Busniess & Service GmbH & Co KG and is released
 * under the terms of CDDL v 1.0 - see cddlv1.0.txt.
 *
 * Feel free to use or modifiy this software under the terms of CDDL v 1.0. However, if you
 * make any modifications, any feedback to the author is appriciated.
 *
 * As this is not a commercial project, only limited support is available under
 *
 * opensource@elaxy.de
 *
 * Version 1.0: 01. July 2012 - Initial public release
 */

package com.elaxy.tools.com.elaxy.tools.scarab2jira;

import java.util.HashMap;

public class UserMappingUtil
{
  private HashMap< String, String > users;

  public UserMappingUtil( String mappingfile, String csvColumn1, String csvColumn2 ) throws SystemException
  {
    users = CsvReader.readCsfFile( mappingfile, csvColumn1, csvColumn2 );
  }

  public String getJiraUserFromScarabUser( String scarabUsername ) throws SystemException
  {
    String jiraUsername = users.get( scarabUsername );
    if ( jiraUsername == null )
    {
      throw new SystemException( "error on mapping scarab username to jira username, no mapping found for scarab username <" + scarabUsername
          + ">" );
    }

    return jiraUsername;
  }
}
