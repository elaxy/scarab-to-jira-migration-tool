/**
 *
 * Scarab to JIRA Migration Tool
 *
 * This software is copyright (c) 2012 ELAXY Busniess & Service GmbH & Co KG and is released
 * under the terms of CDDL v 1.0 - see cddlv1.0.txt.
 *
 * Feel free to use or modifiy this software under the terms of CDDL v 1.0. However, if you
 * make any modifications, any feedback to the author is appriciated.
 *
 * As this is not a commercial project, only limited support is available under
 *
 * opensource@elaxy.de
 *
 * Version 1.0: 01. July 2012 - Initial public release
 */

package com.elaxy.tools.com.elaxy.tools.scarab2jira;

public class ScarabModule
{
  private String name;
  private String description;
  private String owner;

  /*
   * public void dumpContent() { System.out.println( "Scarab Module:" ); System.out.println(
   * "Name        = " + getName() ); System.out.println( "Description = " + getDescription() );
   * System.out.println( "Owner       = " + getOwner()); }
   */

  public void setName( String name )
  {
    this.name = name;
  }

  public String getName()
  {
    return name;
  }

  public void setDescription( String description )
  {
    this.description = description;
  }

  public String getDescription()
  {
    return description;
  }

  public void setOwner( String owner )
  {
    this.owner = owner;
  }

  public String getOwner()
  {
    return owner;
  }
}
