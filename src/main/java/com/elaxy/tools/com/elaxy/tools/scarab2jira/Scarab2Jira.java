/**
 *
 * Scarab to JIRA Migration Tool
 *
 * This software is copyright (c) 2012 ELAXY Busniess & Service GmbH & Co KG and is released
 * under the terms of CDDL v 1.0 - see cddlv1.0.txt.
 *
 * Feel free to use or modifiy this software under the terms of CDDL v 1.0. However, if you
 * make any modifications, any feedback to the author is appriciated.
 *
 * As this is not a commercial project, only limited support is available under
 *
 * opensource@elaxy.de
 *
 * Version 1.0: 01. July 2012 - Initial public release
 */

package com.elaxy.tools.com.elaxy.tools.scarab2jira;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import org.apache.log4j.Logger;

public class Scarab2Jira
{
  private ScarabXmlParser     parser = null;

  private static final Logger logger = Logger.getLogger( Scarab2Jira.class );

  boolean parseScarabXmlfile( String scarabexportfilename )
  {
    // 1. parseScarabXmlInputfile
    logger.debug( "start parsing scarab xml file <" + scarabexportfilename + "> ..." );
    parser = new ScarabXmlParser( scarabexportfilename );
    try
    {
      parser.parse();

      logger.info( "parsed successfully scarab exported xml file for module <" + parser.module.getName() + "/" + parser.module.getDescription()
          + ">, found <" + parser.getNumberOfIssues() + "> issues" );
    }
    catch ( SystemException exc )
    {
      logger.error( exc.getMessage() );
      return false;
    }

    /*
     * for ( ScarabIssue scarabIssue : parser.allIssues ) { scarabIssue.dumpContent(); }
     */

    return true;
  }

  public void checkIfAttachementsAreAvailable( String jirapropertyfile ) throws ConfigurationException, SystemException
  {
    String attachmentRootFolderName = getPropertyFromFile( "attachmentRootFolderName", jirapropertyfile );
    logger.debug( "start checking if all attachments are available in local filesystem ..." );

    int count = 0;
    AttachmentLoader.resetAttachmentCheckedCounter(); 
    
    for ( ScarabIssue scarabIssue : parser.allIssues )
    {
      try
      {
        count++;
        scarabIssue.checkAttachementAvailability( attachmentRootFolderName );        
      }
      catch ( SystemException exc )
      {
        logger.warn( "attachment check: " + exc.getMessage() );
      }
    }
    logger.debug( + AttachmentLoader.getAttachmentCheckedCounter() + " attachments successfully checked for " + count + " scarab issues" );
  }

  boolean importToJira( String jirapropertyfile ) throws IOException
  {
    Import2Jira importScarab2Jira = new Import2Jira();

    try
    {
      importScarab2Jira.startJiraImport( jirapropertyfile, parser );
      logger.info( "jira import successfully finished" );
      return true;
    }
    catch ( SystemException e )
    {
      logger.error( "error on jira import <" + e.getMessage() + ">" );
      return false;
    }
    catch ( ConfigurationException e )
    {
      logger.error( "invalid configuration <" + e.getMessage() + ">" );
      return false;
    }
  }

  private String getPropertyFromFile( String key, String filename ) throws ConfigurationException, SystemException
  {
    // loadProperties
    assert filename != null;

    FileInputStream in = null;
    Properties properties = new Properties();

    try
    {
      in = new FileInputStream( filename );

      if ( filename.endsWith( ".xml" ) )
      {
        properties.loadFromXML( in );
      }
      else
      {
        properties.load( in );
      }
    }
    catch ( FileNotFoundException e )
    {
      throw new SystemException( "Konfigurationsdatei \"" + filename + "\" existiert nicht.", e );
    }
    catch ( InvalidPropertiesFormatException e )
    {
      throw new SystemException( "Konfigurationsdatei \"" + filename + "\" ist ungültig.", e );
    }
    catch ( IOException e )
    {
      throw new SystemException( "Konfigurationsdatei \"" + filename + "\" kann nicht gelesen werden.", e );
    }
    catch ( IllegalArgumentException e )
    {
      throw new SystemException( "Konfigurationsdatei \"" + filename + "\" ist ungültig.", e );
    }
    finally
    {
      if ( in != null )
      {
        try
        {
          in.close();
        }
        catch ( IOException e )
        {
          // Exception deliberately ignored.
        }
      }
    }

    String value = properties.getProperty( key );

    // read single property value
    if ( value == null )
    {
      throw new ConfigurationException( "Erforderlicher Konfigurationsparameter \"" + key + "\" ist nicht angegeben." );
    }

    value = value.trim();

    if ( value.length() == 0 )
    {
      throw new ConfigurationException( "Erforderlicher Konfigurationsparameter \"" + key + "\" beinhaltet eine leere Zeichenkette." );
    }

    return value;
  }
}
