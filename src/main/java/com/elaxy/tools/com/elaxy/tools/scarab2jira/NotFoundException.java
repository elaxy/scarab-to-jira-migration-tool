/**
 *
 * Scarab to JIRA Migration Tool
 *
 * This software is copyright (c) 2012 ELAXY Busniess & Service GmbH & Co KG and is released
 * under the terms of CDDL v 1.0 - see cddlv1.0.txt.
 *
 * Feel free to use or modifiy this software under the terms of CDDL v 1.0. However, if you
 * make any modifications, any feedback to the author is appriciated.
 *
 * As this is not a commercial project, only limited support is available under
 *
 * opensource@elaxy.de
 *
 * Version 1.0: 01. July 2012 - Initial public release
 */

package com.elaxy.tools.com.elaxy.tools.scarab2jira;

public class NotFoundException extends Exception
{
  private static final long serialVersionUID = -3373151889060574035L;

  public NotFoundException()
  {
    super();
  }

  public NotFoundException( String message )
  {
    super( message );
  }

  public NotFoundException( String message, Throwable cause )
  {
    super( message, cause );
  }

  public NotFoundException( Throwable cause )
  {
    super( cause );
  }
}
