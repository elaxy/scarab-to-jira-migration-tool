/**
 *
 * Scarab to JIRA Migration Tool
 *
 * This software is copyright (c) 2012 ELAXY Busniess & Service GmbH & Co KG and is released
 * under the terms of CDDL v 1.0 - see cddlv1.0.txt.
 *
 * Feel free to use or modifiy this software under the terms of CDDL v 1.0. However, if you
 * make any modifications, any feedback to the author is appriciated.
 *
 * As this is not a commercial project, only limited support is available under
 *
 * opensource@elaxy.de
 *
 * Version 1.0: 01. July 2012 - Initial public release
 */

package com.elaxy.tools.com.elaxy.tools.scarab2jira;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public final class CsvReader
{
  private CsvReader()
  {
  }

  public static HashMap< String, String > readCsfFile( String csvmappingFilename, String csvColumnName1, String csvColumnName2 ) throws SystemException
  {
    final String csvSeparator = ",";

    InputStreamReader inputReader =null;
    BufferedReader bufferedReader = null;
    boolean bCsvHeaderParsed = false;

    HashMap< String, String > csvValues = new HashMap< String, String >();

    try
    {
      inputReader = new InputStreamReader( new FileInputStream(csvmappingFilename), "UTF-8");

      bufferedReader = new BufferedReader( inputReader );

      String line = null;

      while ( ( line = bufferedReader.readLine() ) != null )
      {
        // skip comments
        if ( line.startsWith( "#" ) )
        {
          continue;
        }

        String[] splittedLine = line.split( csvSeparator );

        if ( splittedLine.length < 2 || splittedLine.length > 3 )
        {
          throw new SystemException( "invalid csv file <" + csvmappingFilename + ">, line <" + line
              + "> does not contain two required entries separated by '" + csvSeparator + "'" );
        }
        if ( splittedLine.length == 2 )
        {
          if ( !bCsvHeaderParsed )
          {
            if ( splittedLine[ 0 ].trim().compareTo( csvColumnName1.trim() ) != 0 )
            {
              throw new SystemException( "invalid csv file <" + csvmappingFilename + ">, line <" + line
                  + "> does not contain expected header column <" + csvColumnName1 + ">, extracted wrong value <" + splittedLine[ 0 ] + ">" );
            }

            if ( splittedLine[ 1 ].trim().compareTo( csvColumnName2.trim() ) != 0 )
            {
              throw new SystemException( "invalid csv file <" + csvmappingFilename + ">, line <" + line
                  + "> does not contain expected header column <" + csvColumnName2 + ">, extracted wrong value <" + splittedLine[ 0 ] + ">" );
            }

            bCsvHeaderParsed = true;
          }
          else
          {
            csvValues.put( splittedLine[ 0 ].trim(), splittedLine[ 1 ].trim() );
          }
        }
      }
      return csvValues;
    }

    catch ( FileNotFoundException e )
    {
      throw new SystemException( e.getMessage(), e );
    }
    catch ( IOException e )
    {
      throw new SystemException( e.getMessage(), e );

    }
    finally
    {
      if ( bufferedReader != null )
      {
        try
        {
          bufferedReader.close();
        }
        catch ( IOException e )
        {
          // Exception deliberately ignored.
        }
      }

      if ( inputReader != null )
      {
        try
        {
          inputReader.close();
        }
        catch ( IOException ignored )
        {
          // Exception deliberately ignored.
        }
      }
    }
  }
}
