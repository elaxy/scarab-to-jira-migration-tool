/**
 *
 * Scarab to JIRA Migration Tool
 *
 * This software is copyright (c) 2012 ELAXY Busniess & Service GmbH & Co KG and is released
 * under the terms of CDDL v 1.0 - see cddlv1.0.txt.
 *
 * Feel free to use or modifiy this software under the terms of CDDL v 1.0. However, if you
 * make any modifications, any feedback to the author is appriciated.
 *
 * As this is not a commercial project, only limited support is available under
 *
 * opensource@elaxy.de
 *
 * Version 1.0: 01. July 2012 - Initial public release
 */

package com.elaxy.tools.com.elaxy.tools.scarab2jira;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.axis.encoding.Base64;

public final class Base64Encoder
{
  private Base64Encoder()
  {
  }

  public static String encodeFile( File file ) throws IOException
  {
    return Base64.encode( fileToBytes( file ) );
  }

  private static byte[] fileToBytes( File file ) throws IOException
  {
    InputStream is = null;
    try
    {
      is = new FileInputStream( file );

      long length = file.length();

      byte[] bytes = new byte[ ( int ) length ];

      int offset = 0;
      int numRead = 0;
      while ( offset < bytes.length && ( numRead = is.read( bytes, offset, bytes.length - offset ) ) >= 0 )
      {
        offset += numRead;
      }

      if ( offset < bytes.length )
      {
        throw new IOException( "Could not completely read file " + file.getName() );
      }

      return bytes;
    }
    finally
    {
      if ( is != null )
        is.close();
    }

  }

}
