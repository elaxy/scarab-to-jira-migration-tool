/**
 *
 * Scarab to JIRA Migration Tool
 *
 * This software is copyright (c) 2012 ELAXY Busniess & Service GmbH & Co KG and is released
 * under the terms of CDDL v 1.0 - see cddlv1.0.txt.
 *
 * Feel free to use or modifiy this software under the terms of CDDL v 1.0. However, if you
 * make any modifications, any feedback to the author is appriciated.
 *
 * As this is not a commercial project, only limited support is available under
 *
 * opensource@elaxy.de
 *
 * Version 1.0: 01. July 2012 - Initial public release
 */

package com.elaxy.tools.com.elaxy.tools.scarab2jira;

import java.io.File;

public class AttachmentLoader
{  
  private File                attachmentRootFolder;
  private static int attachmentcheckedCounter = 0;

  public AttachmentLoader( String attachmentRootFolderName ) throws SystemException
  {
    // check for valid attachement root path
    attachmentRootFolder = new File( attachmentRootFolderName );
    if ( !attachmentRootFolder.isDirectory() )
    {
      throw new SystemException( "Das Attachement Verzeichnis <" + attachmentRootFolderName + "> existiert nicht." );
    }
  }

  public File getAttachment( String scarabIssueId, String attachmentId, String attachmentFilename ) throws SystemException
  {
    // we have to replace the umlauts in the filenames
    String tmp = attachmentFilename;
    tmp = tmp.replaceAll( "[äöüÄÖÜß?Ã¤]", "_" );
    
    String filename2search = scarabIssueId + "_" + attachmentId + "_" + tmp;   

    return scan4Files( attachmentRootFolder, filename2search );    
  }

  private File scan4Files( File directory, String filename2search ) throws SystemException
  {
    // check for valid path
    if ( !directory.isDirectory() )
    {
      throw new SystemException( "Das Verzeichnis <" + directory.getAbsolutePath() + "> existiert nicht." );
    }

    // logger.debug( "enter folder : " + directory.getAbsolutePath() );
    File[] fileList = directory.listFiles(); // read all files
    if ( fileList == null )
    {
      // "keine Files gefunden im aktuellen Verzeichnis
      return null;
    }

    for ( int i = 0; i < fileList.length; i++ )
    {
      File myFile = fileList[ i ];      
      if ( myFile.isDirectory() )
      {
        File temp = scan4Files( myFile, filename2search );
        if ( temp != null )
        {
          return temp;
        }
      }

      // check if file is requested file
      if ( myFile.getName().equals( filename2search ) )
      {
        return myFile;
      }
    }

    // nothing found
    return null;
    // logger.debug( "leave folder : " + directory.getAbsolutePath() );
  }
  
  public static void resetAttachmentCheckedCounter()
  {
    attachmentcheckedCounter = 0;
  }
  
  public static void increaseAttachmentCheckedCounter()
  {
    attachmentcheckedCounter++;
  }
  
  public static int getAttachmentCheckedCounter()
  {
   return attachmentcheckedCounter;
  }  
}
