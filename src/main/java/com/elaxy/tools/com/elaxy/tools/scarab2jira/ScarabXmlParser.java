/**
 *
 * Scarab to JIRA Migration Tool
 *
 * This software is copyright (c) 2012 ELAXY Busniess & Service GmbH & Co KG and is released
 * under the terms of CDDL v 1.0 - see cddlv1.0.txt.
 *
 * Feel free to use or modifiy this software under the terms of CDDL v 1.0. However, if you
 * make any modifications, any feedback to the author is appriciated.
 *
 * As this is not a commercial project, only limited support is available under
 *
 * opensource@elaxy.de
 *
 * Version 1.0: 01. July 2012 - Initial public release
 */

package com.elaxy.tools.com.elaxy.tools.scarab2jira;

import java.io.IOException;
import java.util.Vector;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class ScarabXmlParser
{
  public ScarabModule          module    = new ScarabModule();
  public Vector< ScarabIssue > allIssues = new Vector< ScarabIssue >();
  private String               filename  = null;

  public ScarabXmlParser( String filename )
  {
    this.filename = filename;
  }

  public void parse() throws SystemException
  {
    // System.getProperties().put("http.proxyHost", "proxy.elaxy.org");
    // System.getProperties().put("http.proxyPort", "3128" );

    try
    {
      // Parse a XML File
      DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
      Document doc = docBuilder.parse( filename );

      // the root element
      doc.getDocumentElement().normalize();

      // the module Length
      NodeList moduleNodeList = doc.getElementsByTagName( "module" );

      // the Number of Issue
      NodeList issueNodeList = doc.getElementsByTagName( "issue" );

      if ( moduleNodeList.getLength() != 1 )
      {
        throw new SystemException( "invalid number of '<module>' entires in file, expected only one." );
      }

      Node firstModuleNode = moduleNodeList.item( 0 );
      if ( firstModuleNode.getNodeType() == Node.ELEMENT_NODE )
      {
        module.setName( extractFirstElement( firstModuleNode, "name", true ) );
        module.setDescription( extractFirstElement( firstModuleNode, "description", true ) );
        module.setOwner( extractFirstElement( firstModuleNode, "owner", true ) );
      }// end If for Module Node

      // cycle over all issues
      for ( int i = 0; i < issueNodeList.getLength(); i++ )
      {
        Node issueNode = issueNodeList.item( i );
        if ( issueNode.getNodeType() == Node.ELEMENT_NODE )
        {
          ScarabIssue issue = new ScarabIssue();
          issue.setIssueID( extractFirstElement( issueNode, "id", true ) );
          issue.setArtifactType( extractFirstElement( issueNode, "artifact-type", true ) );

          Node activitySetsNode = findFirstElementNode( issueNode, "activity-sets" );
          if ( activitySetsNode != null )
          {
            Vector< Node > activitySetNodes = findAllElementNode( activitySetsNode, "activity-set" );
            for ( Node node : activitySetNodes )
            {
              ScarabActivitySet scarabActivitySet = new ScarabActivitySet();
              scarabActivitySet.setId( Integer.parseInt( extractFirstElement( node, "id", true ) ) );
              scarabActivitySet.setType( extractFirstElement( node, "type", true ) );
              scarabActivitySet.setEditor( extractFirstElement( node, "created-by", true ) );
              scarabActivitySet.setTimestamp( extractFirstElement( findFirstElementNode( node, "created-date" ), "timestamp", true ) );

              Node attachmentNode = findFirstElementNode( node, "attachment" );
              if ( attachmentNode != null )
              {
                String type = extractFirstElement( attachmentNode, "type", true );
                if ( type.equalsIgnoreCase( "MODIFICATION" ) || type.equalsIgnoreCase( "COMMENT" ) )
                {
                  scarabActivitySet.setComment( extractFirstElement( attachmentNode, "data", false ) );
                }
              }

              Vector< Node > activityNodes = findAllElementNode( findFirstElementNode( node, "activities" ), "activity" );
              for ( Node activityNode : activityNodes )
              {
                ScarabActivity scarabActivity = new ScarabActivity();
                scarabActivity.setId( Integer.parseInt( extractFirstElement( activityNode, "id", true ) ) );
                scarabActivity.setAttribute( extractFirstElement( activityNode, "attribute", true ) );
                scarabActivity.setDescription( extractFirstElement( activityNode, "description", false ) );
                scarabActivity.setNewValue( extractFirstElement( activityNode, "new-value", false ) );
                scarabActivity.setOldValue( extractFirstElement( activityNode, "old-value", false ) );
                scarabActivity.setCommentBody( extractFirstElement( activityNode, "data", false ) );
                scarabActivity.setCommentName( extractFirstElement( activityNode, "created-by", false ) );
                scarabActivity.setAttachmentType( extractFirstElement( activityNode, "type", false ) );
                
                Node activityAttachmentNode = findFirstElementNode( activityNode, "attachment" );
                if ( activityAttachmentNode != null )
                {
                  scarabActivity.setAttachmentId( extractFirstElement(activityAttachmentNode, "id", false) );
                  scarabActivity.setAttachmentFilename( extractFirstElement(activityAttachmentNode, "filename", false) );
                  if (extractFirstElement(activityAttachmentNode, "deleted", false).equals( "true" ))
                  {
                    scarabActivity.setDeleted( true );
                  }
                }

                Node dependencyNode = findFirstElementNode( activityNode, "dependency" );
                if ( dependencyNode != null )
                {
                  scarabActivity.setDependencyType( extractFirstElement( dependencyNode, "type", true ) );
                  scarabActivity.setParent( extractFirstElement( dependencyNode, "parent", true ) );
                  scarabActivity.setChild( extractFirstElement( dependencyNode, "child", true ) );
                  if (extractFirstElement(dependencyNode, "deleted", false).equals( "true" ))
                  {
                    scarabActivity.setDeleted( true );
                  }
                }
                
                scarabActivitySet.addActivitySet( scarabActivity );
              }
              issue.addActivitySet( scarabActivitySet );
            }
          }
          allIssues.add( issue );
        }
      }// end if issue cycle
    }// end try
    catch ( SAXParseException exc )
    {
      throw new SystemException( "parse error in line " + exc.getLineNumber() + ", uri " + exc.getSystemId(), exc );
    }
    catch ( SAXException exc )
    {
      throw new SystemException( "parse error, reason: " + exc.getMessage(), exc );
    }
    catch ( ParserConfigurationException exc )
    {
      throw new SystemException( "parser configuration error, reason: " + exc.getMessage(), exc );
    }
    catch ( IOException exc )
    {
      throw new SystemException( "file i/o error " + exc.getMessage(), exc );
    }
  }

  public int getNumberOfIssues()
  {
    return this.allIssues.size();
  }

  private Node findFirstElementNode( Node node, String key )
  {
    for ( int i = 0; i < node.getChildNodes().getLength(); i++ )
    {
      if ( node.getChildNodes().item( i ).getNodeType() != Node.ELEMENT_NODE )
      {
        continue;
      }

      if ( node.getChildNodes().item( i ).getNodeName().compareTo( key ) == 0 )
      {
        return node.getChildNodes().item( i );
      }
    }
    return null;
  }

  private Vector< Node > findAllElementNode( Node node, String key )
  {
    Vector< Node > nodes = new Vector< Node >();

    for ( int i = 0; i < node.getChildNodes().getLength(); i++ )
    {
      if ( node.getChildNodes().item( i ).getNodeType() != Node.ELEMENT_NODE )
      {
        continue;
      }

      if ( node.getChildNodes().item( i ).getNodeName().compareTo( key ) == 0 )
      {
        nodes.add( node.getChildNodes().item( i ) );
      }
    }

    return nodes;
  }

  private String extractFirstElement( Node node, String key, boolean mandatory ) throws SystemException
  {
    String result = "";
    Element element = ( Element ) node;
    NodeList nodeList = element.getElementsByTagName( key );
    Element firstElement = ( Element ) nodeList.item( 0 );

    if ( firstElement == null )
    {
      if ( mandatory )
      {
        throw new SystemException( "parse error: key <" + key + "> not found in node <" + node.getNodeName() + ">" + " context<"
            + node.getTextContent() + ">" );
      }

      // values could not be extracted, but the values is optional
      return "";
    }
    NodeList childNodeList = firstElement.getChildNodes();
    
    if ( childNodeList.item( 0 ) != null)
    {
      result = childNodeList.item( 0 ).getNodeValue();
    }
    return result.trim();
  }

  // private void dumpNodes( NodeList nodeList )
  // {
  // System.out.println( "Number of Nodes = " + nodeList.getLength() );
  // for ( int xx = 0; xx < nodeList.getLength(); xx++ )
  // {
  // System.out.println( "Node(" + xx + ") = " + nodeList.item( xx ).getNodeName() + " / " +
  // nodeList.item( xx ).getNodeType() + " / "
  // + nodeList.item( xx ).getNodeValue() );
  // }
  // }
}
