/**
 *
 * Scarab to JIRA Migration Tool
 *
 * This software is copyright (c) 2012 ELAXY Busniess & Service GmbH & Co KG and is released
 * under the terms of CDDL v 1.0 - see cddlv1.0.txt.
 *
 * Feel free to use or modifiy this software under the terms of CDDL v 1.0. However, if you
 * make any modifications, any feedback to the author is appriciated.
 *
 * As this is not a commercial project, only limited support is available under
 *
 * opensource@elaxy.de
 *
 * Version 1.0: 01. July 2012 - Initial public release
 */

package com.elaxy.tools.com.elaxy.tools.scarab2jira;

public class ScarabActivity
{
  private int    id;
  private String attribute;
  private String newValue;
  private String oldValue;
  private String description;
  private String dependencyType;
  private String attachmentType;
  private String commentBody;
  private String commentName;
  private String parent;
  private String child;
  private String attachmentId;
  private String attachmentFilename;
  private boolean isDeleted;

  /*
   * public void dumpContent() { System.out.println( "Scarab Activity:" ); System.out.println(
   * "Id          = " + getId() ); System.out.println( "Attribute   = " + getAttribute() );
   * System.out.println( "NewValue    = " + getNewValue() ); System.out.println( "Description = " +
   * getDescription() ); }
   */

  public void setId( int id )
  {
    this.id = id;
  }

  public int getId()
  {
    return id;
  }

  public void setAttribute( String attribute )
  {
    this.attribute = attribute;
  }

  public String getAttribute()
  {
    return attribute;
  }

  public void setNewValue( String newValue )
  {
    this.newValue = newValue;
  }

  public String getNewValue()
  {
    return newValue;
  }

  public void setOldValue( String oldValue )
  {
    this.oldValue = oldValue;
  }

  public String getOldValue()
  {
    return oldValue;
  }

  public void setDescription( String description )
  {
    this.description = description;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDependencyType( String dependencyType )
  {
    this.dependencyType = dependencyType;
  }

  public String getDependencyType()
  {
    return dependencyType;
  }

  public void setCommentBody( String commentBody )
  {
    this.commentBody = commentBody;
  }

  public String getCommentBody()
  {
    return commentBody;
  }

  public void setCommentName( String commentName )
  {
    this.commentName = commentName;
  }

  public String getCommentName()
  {
    return commentName;
  }

  public void setAttachmentType( String attachmentType )
  {
    this.attachmentType = attachmentType;
  }

  public String getAttachmentType()
  {
    return attachmentType;
  }

  public void setParent( String parent )
  {
    this.parent = parent;
  }

  public String getParent()
  {
    return parent;
  }

  public void setChild( String child )
  {
    this.child = child;
  }

  public String getChild()
  {
    return child;
  }

  public void setAttachmentId( String attachmentId )
  {
    this.attachmentId = attachmentId;
  }

  public String getAttachmentId()
  {
    return attachmentId;
  }

  public void setAttachmentFilename( String filename )
  {
    this.attachmentFilename = filename;  
  }

  public String getAttachmentFilename()
  {
    return attachmentFilename;
  }

  public void setDeleted( boolean isDeleted )
  {
    this.isDeleted = isDeleted;
  }

  public boolean isDeleted()
  {
    return isDeleted;
  }

}
