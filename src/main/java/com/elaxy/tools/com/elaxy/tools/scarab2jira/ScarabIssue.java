/**
 *
 * Scarab to JIRA Migration Tool
 *
 * This software is copyright (c) 2012 ELAXY Busniess & Service GmbH & Co KG and is released
 * under the terms of CDDL v 1.0 - see cddlv1.0.txt.
 *
 * Feel free to use or modifiy this software under the terms of CDDL v 1.0. However, if you
 * make any modifications, any feedback to the author is appriciated.
 *
 * As this is not a commercial project, only limited support is available under
 *
 * opensource@elaxy.de
 *
 * Version 1.0: 01. July 2012 - Initial public release
 */

package com.elaxy.tools.com.elaxy.tools.scarab2jira;

import java.util.Vector;

public class ScarabIssue
{
  private String                      issueID;
  private String                      artifactType;
  private Vector< ScarabActivitySet > scarabActivitySets = new Vector< ScarabActivitySet >();

  /*
   * public void dumpContent() { System.out.println( "Scarab Issue:" ); System.out.println(
   * "IssueID      = " + this.getIssueID() ); System.out.println( "ArtifactType = " +
   * this.getArtifactType() ); for ( ScarabActivitySet myAcSet : scarabActivitySets ) {
   * myAcSet.dumpContent(); } }
   */

  public void setIssueID( String issueID )
  {
    this.issueID = issueID;
  }

  public String getIssueID()
  {
    return issueID;
  }

  public void setArtifactType( String artifactType )
  {
    this.artifactType = artifactType;
  }

  public String getArtifactType()
  {
    return artifactType;
  }

  public void addActivitySet( ScarabActivitySet activitySet )
  {
    this.scarabActivitySets.add( activitySet );
  }

  public ScarabActivitySet getActivitySet( int i )
  {
    return scarabActivitySets.get( i );
  }

  public Vector< ScarabActivitySet > getActivitySetVector()
  {
    return scarabActivitySets;
  }
  
  public void checkAttachementAvailability( String attachmentRootFolderName ) throws SystemException
  {
    // go over all activity sets
    for ( ScarabActivitySet activitySet : scarabActivitySets )
    {
      activitySet.checkAttachementAvailability( getIssueID(), attachmentRootFolderName );
    }        
  }
}
