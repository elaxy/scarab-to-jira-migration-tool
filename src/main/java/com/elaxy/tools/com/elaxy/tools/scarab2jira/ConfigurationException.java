/**
 *
 * Scarab to JIRA Migration Tool
 *
 * This software is copyright (c) 2012 ELAXY Busniess & Service GmbH & Co KG and is released
 * under the terms of CDDL v 1.0 - see cddlv1.0.txt.
 *
 * Feel free to use or modifiy this software under the terms of CDDL v 1.0. However, if you
 * make any modifications, any feedback to the author is appriciated.
 *
 * As this is not a commercial project, only limited support is available under
 *
 * opensource@elaxy.de
 *
 * Version 1.0: 01. July 2012 - Initial public release
 */

package com.elaxy.tools.com.elaxy.tools.scarab2jira;

public class ConfigurationException extends Exception
{
  private static final long serialVersionUID = -3763962167686913365L;

  public ConfigurationException()
  {
    super();
  }

  public ConfigurationException( String message )
  {
    super( message );
  }

  public ConfigurationException( String message, Throwable cause )
  {
    super( message, cause );
  }

  public ConfigurationException( Throwable cause )
  {
    super( cause );
  }
}
