/**
 *
 * Scarab to JIRA Migration Tool
 *
 * This software is copyright (c) 2012 ELAXY Busniess & Service GmbH & Co KG and is released
 * under the terms of CDDL v 1.0 - see cddlv1.0.txt.
 *
 * Feel free to use or modifiy this software under the terms of CDDL v 1.0. However, if you
 * make any modifications, any feedback to the author is appriciated.
 *
 * As this is not a commercial project, only limited support is available under
 *
 * opensource@elaxy.de
 *
 * Version 1.0: 01. July 2012 - Initial public release
 */

package com.elaxy.tools.com.elaxy.tools.scarab2jira;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MappingUtil
{
  private HashMap< String, String > mapping;
  private HashMap< String, String > mappingid;
  private String                    description;

  public MappingUtil( String description, String mappingfile, String file1csvColumn1, String file1csvColumn2, String mappingidfile, String file2csvColumn1, String file2csvColumn2 ) throws SystemException
  {
    mapping = CsvReader.readCsfFile( mappingfile, file1csvColumn1, file1csvColumn2 );
    mappingid = CsvReader.readCsfFile( mappingidfile, file2csvColumn1, file2csvColumn2 );

    // additional check if second value in map mappingid is unique
    Iterator it = mappingid.entrySet().iterator();
    HashMap< String, String > uniqueEntryCheckMap = new HashMap< String, String >();

    while ( it.hasNext() )
    {
      Map.Entry entry = ( Map.Entry ) it.next();
      String key = ( String ) entry.getValue();
      if ( uniqueEntryCheckMap.containsKey( key ) )
      {
        throw new SystemException( "entry in column <" + file2csvColumn2 + "> is not configured unique" );
      }
      uniqueEntryCheckMap.put( key, "" );
    }

    this.description = description;
  }

  public String getJiraValueIdFromScarabValue( String scarabValue ) throws SystemException
  {
    String jiraValue = mapping.get( scarabValue );
    if ( jiraValue == null )
    {
      throw new SystemException( "error on mapping scarab " + description + " to jira " + description + ", no mapping found for scarab "
          + description + " <" + scarabValue + ">" );
    }

    String jiraValueId = mappingid.get( jiraValue );
    if ( jiraValueId == null )
    {
      throw new SystemException( "error on mapping scarab " + description + " to jira " + description + ", no mapping found for jira "
          + description + " <" + jiraValue + ">" );
    }

    return jiraValueId;
  }
}

/*
 * public class MappingUtil { private HashMap< String, String > mappingstatus; private HashMap<
 * String, String > mappingstatusid;
 * 
 * private HashMap< String, String > mappingpriority; private HashMap< String, String >
 * mappingpriorityid;
 * 
 * private HashMap< String, String > mappingsolution; private HashMap< String, String >
 * mappingsolutionid;
 * 
 * private HashMap< String, String > mappingtype; private HashMap< String, String > mappingtypeid;
 * 
 * private HashMap< String, String > mappinguser;
 * 
 * 
 * public MappingUtil( String mappingstatusfile, String mappingstatusidfile, String
 * mappingpriorityfile, String mappingpriorityidfile, String mappingsolutionfile, String
 * mappingsolutionidfile, String mappingtypefile, String mappingtypeidfile, String mappinguserfile )
 * throws SystemException { mappingstatus = CsvReader.readCsfFile( mappingstatusfile,
 * "ScarabStatus", "JiraStatus" ); mappingstatusid = CsvReader.readCsfFile( mappingstatusidfile,
 * "JiraStatus", "JiraID" );
 * 
 * mappingpriority = CsvReader.readCsfFile( mappingpriorityfile, "ScarabPriority", "JiraPriority" );
 * mappingpriorityid = CsvReader.readCsfFile( mappingpriorityidfile, "JiraPriority", "JiraID" );
 * 
 * mappingsolution = CsvReader.readCsfFile( mappingsolutionfile, "ScarabSolution", "JiraId" );
 * mappingsolutionid = CsvReader.readCsfFile( mappingsolutionidfile, "JiraSolution", "JiraID" );
 * 
 * mappingtype = CsvReader.readCsfFile( mappingtypefile, "ScarabType", "JiraVorgangstyp" );
 * mappingtypeid = CsvReader.readCsfFile( mappingtypeidfile, "JiraType", "JiraID" );
 * 
 * mappinguser = CsvReader.readCsfFile( mappinguserfile, "ScarabUser", "JiraUser" ); }
 * 
 * public String getJiraStatusIdFromScarabStatus( String scarabStatus ) throws SystemException {
 * String jiraStatus = mappingstatus.get( scarabStatus ); if ( jiraStatus == null ) { throw new
 * SystemException(
 * "error on mapping scarab status to jira status, no mapping found for scarab status <" +
 * scarabStatus + ">" ); }
 * 
 * String jiraStatusId = mappingstatusid.get( jiraStatus ); if ( jiraStatusId == null ) { throw new
 * SystemException(
 * "error on mapping scarab status to jira status, no mapping found for jira status <" + jiraStatus
 * + ">" ); }
 * 
 * return jiraStatusId; } }
 */
