/**
 *
 * Scarab to JIRA Migration Tool
 *
 * This software is copyright (c) 2012 ELAXY Busniess & Service GmbH & Co KG and is released
 * under the terms of CDDL v 1.0 - see cddlv1.0.txt.
 *
 * Feel free to use or modifiy this software under the terms of CDDL v 1.0. However, if you
 * make any modifications, any feedback to the author is appriciated.
 *
 * As this is not a commercial project, only limited support is available under
 *
 * opensource@elaxy.de
 *
 * Version 1.0: 01. July 2012 - Initial public release
 */

package com.elaxy.tools.com.elaxy.tools.scarab2jira;

import java.io.File;
import java.util.Vector;

public class ScarabActivitySet
{
  private int                      id;
  private String                   type;
  private String                   editor;
  private String                   timestamp;
  private String                   comment;
  private Vector< ScarabActivity > scarabActivitys = new Vector< ScarabActivity >();

  /*
   * public void dumpContent() { System.out.println( "Scarab Activity Set:" ); System.out.println(
   * "Id        = " + getId() ); System.out.println( "Type      = " + getType() );
   * System.out.println( "Editor    = " + getEditor() ); System.out.println( "Timestamp = " +
   * getTimestamp() ); for ( ScarabActivity activity : scarabActivitys ) { activity.dumpContent(); }
   * }
   */

  public void setId( int id )
  {
    this.id = id;
  }

  public int getId()
  {
    return id;
  }

  public void setType( String type )
  {
    this.type = type;
  }

  public String getType()
  {
    return type;
  }

  public void setEditor( String editor )
  {
    this.editor = editor;
  }

  public String getEditor()
  {
    return editor;
  }

  public void setTimestamp( String timestamp )
  {
    this.timestamp = timestamp;
  }

  public String getTimestamp()
  {
    return timestamp;
  }

  public void setComment( String comment )
  {
    this.comment = comment;
  }

  public String getComment()
  {
    return this.comment;
  }

  public void addActivitySet( ScarabActivity activity )
  {
    this.scarabActivitys.add( activity );
  }

  public ScarabActivity getActivity( int i )
  {
    return scarabActivitys.get( i );
  }

  public Vector< ScarabActivity > getActivityVector()
  {
    return scarabActivitys;
  }

  public void checkAttachementAvailability( String scarabIssueID, String attachmentRootFolderName ) throws SystemException
  {
    // check if attachment is available in local filesystem (we do not want to run into attachement
    // read errors in further processing steps)
    AttachmentLoader attachmentLoader = new AttachmentLoader( attachmentRootFolderName );
    
    for ( ScarabActivity scarabActivity : scarabActivitys )
    {
      
      if ( scarabActivity.getAttachmentFilename() == null || scarabActivity.getAttachmentFilename().isEmpty() )
      {
        continue; // noting to do
      }     
      
      File attachment = attachmentLoader.getAttachment( scarabIssueID, scarabActivity.getAttachmentId(), scarabActivity.getAttachmentFilename() );
      if ( attachment != null )
      {
        AttachmentLoader.increaseAttachmentCheckedCounter();
      }
      else
      {
        throw new SystemException( "attachment not found in filesystem, scarabIssueID<" + scarabIssueID + "> AttachmentId<" + scarabActivity.getAttachmentId() + "> AttachmentFilename <" + scarabActivity.getAttachmentFilename() +">" );
      }
    }
  }
}
