/**
 *
 * Scarab to JIRA Migration Tool
 *
 * This software is copyright (c) 2012 ELAXY Busniess & Service GmbH & Co KG and is released
 * under the terms of CDDL v 1.0 - see cddlv1.0.txt.
 *
 * Feel free to use or modifiy this software under the terms of CDDL v 1.0. However, if you
 * make any modifications, any feedback to the author is appriciated.
 *
 * As this is not a commercial project, only limited support is available under
 *
 * opensource@elaxy.de
 *
 * Version 1.0: 01. July 2012 - Initial public release
 */

package com.elaxy.tools.com.elaxy.tools.scarab2jira;

public class JiraUser
{
  private String scarabUsername;
  private String jiraUsername;
  private String jiraPassword;

  public JiraUser()
  {
  }

  public JiraUser( String jiraUsername, String jiraPassword )
  {
    this.jiraUsername = jiraUsername;
    this.jiraPassword = jiraPassword;
  }

  public void setScarabUsername( String scarabUsername )
  {
    this.scarabUsername = scarabUsername;
  }

  public String getScarabUsername()
  {
    return scarabUsername;
  }

  public void setJiraUsername( String jiraUsername )
  {
    this.jiraUsername = jiraUsername;
  }

  public String getJiraUsername()
  {
    return jiraUsername;
  }

  public void setJiraPassword( String jiraPassword )
  {
    this.jiraPassword = jiraPassword;
  }

  public String getJiraPassword()
  {
    return jiraPassword;
  }
}
