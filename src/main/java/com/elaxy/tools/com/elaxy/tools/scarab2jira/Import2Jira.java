/**
 *
 * Scarab to JIRA Migration Tool
 *
 * This software is copyright (c) 2012 ELAXY Busniess & Service GmbH & Co KG and is released
 * under the terms of CDDL v 1.0 - see cddlv1.0.txt.
 *
 * Feel free to use or modifiy this software under the terms of CDDL v 1.0. However, if you
 * make any modifications, any feedback to the author is appriciated.
 *
 * As this is not a commercial project, only limited support is available under
 *
 * opensource@elaxy.de
 *
 * Version 1.0: 01. July 2012 - Initial public release
 */

package com.elaxy.tools.com.elaxy.tools.scarab2jira;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;
import java.util.Vector;

import javax.xml.rpc.ServiceException;

import org.apache.log4j.Logger;

import com.elaxy.generated.jirastubs.JiraSoapService;
import com.elaxy.generated.jirastubs.JiraSoapServiceServiceLocator;
import com.elaxy.generated.jirastubs.RemoteAuthenticationException;
import com.elaxy.generated.jirastubs.RemoteComment;
import com.elaxy.generated.jirastubs.RemoteFieldValue;
import com.elaxy.generated.jirastubs.RemoteIssue;
import com.elaxy.generated.jirastubs.RemotePermissionException;
import com.elaxy.generated.jirastubs.RemotePermissionScheme;
import com.elaxy.generated.jirastubs.RemoteProject;
import com.elaxy.generated.jirastubs.RemoteValidationException;
import com.elaxy.generated.jirastubs.RemoteException;

public class Import2Jira
{
  private Properties                    myproperties;
  private static final Logger           logger                     = Logger.getLogger( Scarab2Jira.class );
  private JiraSoapServiceServiceLocator locator                    = null;
  // private JiraUser defaultJiraUser; // we do not require a default jira user, if someone should
  // not be mapped we can configure this in the user mapping file
  private MappingUtil                   mappingUtilPriority;
  private MappingUtil                   mappingUtilType;
  private MappingUtil                   mappingUtilSolution;

  private int                           prevNo                     = 0;
  private int                           currentNo                  = 0;
  private int                           startNo;

  private AttachmentLoader              attachmentLoader;

  private Vector< String >              dependencies               = new Vector< String >();
  private Vector< String >              modificationComments       = new Vector< String >();
  private Vector< String >              changes                    = new Vector< String >();
  private Vector< String >              assignees                  = new Vector< String >();
  private Vector< String >              comments                   = new Vector< String >();
  private Vector< String >              attachmentFilenames        = new Vector< String >();
  private Vector< String >              attachmentData             = new Vector< String >();
  private Vector< String >              notImportedAttachments     = new Vector< String >();
  private Vector[]                      attachmentVectors          = { attachmentFilenames, attachmentData };
  private HashMap< String, String >     additionalScarabAttributes = new HashMap< String, String >();
  private HashMap< String, Vector[] >   attachments                = new HashMap< String, Vector[] >();

  private void readAllCsvConfigurations() throws ConfigurationException, SystemException
  {
    mappingUtilPriority = new MappingUtil( "priority", getPropertyAsString( "mappingpriorityfile" ), "ScarabPriority", "JiraPriority",
        getPropertyAsString( "mappingpriorityidfile" ), "JiraPriority", "JiraID" );

    mappingUtilType = new MappingUtil( "type", getPropertyAsString( "mappingtypefile" ), "ScarabType", "JiraVorgangstyp",
        getPropertyAsString( "mappingtypeidfile" ), "JiraType", "JiraID" );

    mappingUtilSolution = new MappingUtil( "solution", getPropertyAsString( "mappingsolutionfile" ), "ScarabSolution", "JiraSolution",
        getPropertyAsString( "mappingsolutionidfile" ), "JiraSolution", "JiraID" );

    attachmentLoader = new AttachmentLoader( getPropertyAsString( "attachmentRootFolderName" ) );
    // String defaultUser = getPropertyAsString( "jiradefaultuser" );
    // String defaultUserpasswd = getPropertyAsString( "jiradefaultpassword" );
    // defaultJiraUser = new JiraUser( defaultUser, defaultUserpasswd );
  }

  public void startJiraImport( String jirapropertyfile, ScarabXmlParser parser ) throws SystemException, ConfigurationException, IOException
  {
    if ( parser == null )
    {
      throw new SystemException( "no parsed scarab data available." );
    }

    myproperties = loadProperties( jirapropertyfile );
    readAllCsvConfigurations();

    locator = new JiraSoapServiceServiceLocator();

    try
    {
      JiraSoapService remote = locator.getJirasoapserviceV2();
      String token = remote.login( getPropertyAsString( "jirauser" ), getPropertyAsString( "jirapassword" ) );
      // log in at JIRA
      remote.login( getPropertyAsString( "jirauser" ), getPropertyAsString( "jirapassword" ) );
      logger.debug( "successfully loggedin at JIRA server" );
      createNewJiraProjectIfRequired( parser, remote, token );

      // Get the Project from JIRA
      String key = getPropertyAsString( "jiraprojectkey" );
      RemoteProject project = remote.getProjectByKey( token, key );
      dumpContentProject( project );

      String[] attributes = getPropertyAsString( "additionalScarabAttributes" ).split( ";" );

      for ( String attribute : attributes )
      {
        additionalScarabAttributes.put( attribute, "k.A." );
      }
      additionalScarabAttributes.put( "Zugewiesen", "k.A." );

      // loop all Issues
      boolean issueWasCreated = false;

      for ( int i = 0; i < parser.allIssues.size(); i++ )
      {
        ScarabIssue scarabIssue = parser.allIssues.get( i );
        // for the dummies
        setIndexOfIssues( scarabIssue.getIssueID() );
        if ( ( prevNo + 1 ) != currentNo )
        {
          int tmp = currentNo - ( prevNo + 1 );
          for ( int a = 0; a < tmp; a++ )
          {
            createJIRADummy( ( prevNo + 1 ), remote, token, key );
          }
        }
        RemoteIssue issue = new RemoteIssue();

        issue.setProject( project.getKey() );

        // We don't want to migrate the tickets of type "MOM Requirement", "Requirement" or "Task"
        if ( scarabIssue.getArtifactType().equalsIgnoreCase( "MOM Requirement" )
            || ( this.getPropertyAsString( "scarabprojectticketidentifier" ).equals( "DAP" ) && ( scarabIssue.getArtifactType().equalsIgnoreCase(
                "Requirement" ) || scarabIssue.getArtifactType().equalsIgnoreCase( "Task" ) ) ) )
        {
          createJIRADummy( ( prevNo + 1 ), remote, token, key );
          continue;
        }

        issue.setType( issueType( scarabIssue.getArtifactType() ) );
        issue.setKey( key + "-" + currentNo );

        for ( int j = 0; j < scarabIssue.getActivitySetVector().size(); j++ )
        {
          ScarabActivitySet scarabActivitySet = scarabIssue.getActivitySet( j );

          this.extractScarabAttributes( scarabActivitySet, issue, scarabIssue.getIssueID() );

        }

        if ( attachmentFilenames.size() != 0 )
        {
          attachments.put( scarabIssue.getIssueID(), attachmentVectors );
        }

        issueWasCreated = this.createAndUpdateJiraIssue( issue, token, remote, scarabIssue.getIssueID() );

        String assignee = "";
        if ( assignees.size() == 0 )
        {
          assignee = "k.A.";
        }

        StringBuffer tmp = new StringBuffer();
        for ( int index = 0; index < assignees.size(); index++ )
        {
          tmp.append( assignees.get( index ) );
          if ( index != ( assignees.size() - 1 ) )
          {
            tmp.append( ", " );
          }
        }
        if ( tmp.length() > 0 )
        {
          assignee = tmp.toString();
        }

        additionalScarabAttributes.put( "Zugewiesen", assignee );

        if ( issueWasCreated )
        {
          this.addCommentWithChangeHistory( token, issue, remote );
        }

        dependencies.clear();
        modificationComments.clear();
        changes.clear();
        comments.clear();
        assignees.clear();
        attachmentFilenames.clear();
        attachmentData.clear();
        notImportedAttachments.clear();
        for ( String attribute : attributes )
        {
          additionalScarabAttributes.put( attribute, "k.A." );
        }
      }
      logger.info( "All " + parser.allIssues.size() + " Issues from module " + parser.module.getName() + " imported to JIRA!" );
      if ( remote.logout( token ) )
      {
        logger.debug( "successfully logged out" );
      }
    }
    catch ( RemoteAuthenticationException exc )
    {
      throw new SystemException( "login at JIRA server failed, reason: invalid authentification(username/password)", exc );
    }
    catch ( ServiceException exc )
    {
      throw new SystemException( "Service Exceptionlogin at JIRA server failed, reason: " + exc.getMessage(), exc );
    }
    catch ( RemoteException exc )
    {
      throw new SystemException( "Remote Exception login at JIRA server failed, reason: remote error " + exc.getMessage() + ", reason: "
          + exc.getFaultReason(), exc );
    }
    catch ( java.rmi.RemoteException exc )
    {
      throw new SystemException( "login at JIRA server failed, reason: remote error " + exc.getMessage(), exc );
    }
  }

  private void createNewJiraProjectIfRequired( ScarabXmlParser parser, JiraSoapService remote, String sessionid ) throws ConfigurationException, java.rmi.RemoteException
  {
    if ( getPropertyAsString( "jiraprojectcreate" ).equalsIgnoreCase( "yes" ) )
    {
      logger.info( "Project <" + getPropertyAsString( "jiraprojectname" ) + "> does not exist it will be created" );
      RemoteProject project = new RemoteProject();
      project.setKey( getPropertyAsString( "jiraprojectkey" ) );
      project.setName( getPropertyAsString( "jiraprojectname" ) );
      project.setDescription( parser.module.getDescription() );

      project.setLead( getPropertyAsString( "jiraprojectlead" ) );

      // Default PermissionScheme for Projects: Standardberechtigunsschema
      RemotePermissionScheme permissionScheme = remote.getPermissionSchemes( sessionid )[ 0 ];
      permissionScheme.getDescription();
      project.setPermissionScheme( permissionScheme );
      remote.createProjectFromObject( sessionid, project );
      logger.info( "sucessfully create JIRA Project <" + getPropertyAsString( "jiraprojectname" ) + ">" );
    }
    else
    {
      String jqlQuery = "project=" + getPropertyAsString( "jiraprojectname" );
      startNo = remote.getIssuesFromJqlSearch( sessionid, jqlQuery, 10000 ).length;
      currentNo = startNo;
    }
  }

  private Properties loadProperties( String filename ) throws SystemException
  {
    assert filename != null;

    FileInputStream in = null;
    Properties properties = new Properties();

    try
    {
      in = new FileInputStream( filename );

      if ( filename.endsWith( ".xml" ) )
      {
        properties.loadFromXML( in );
      }
      else
      {
        properties.load( in );
      }
      return properties;
    }
    catch ( FileNotFoundException e )
    {
      throw new SystemException( "Konfigurationsdatei \"" + filename + "\" existiert nicht.", e );
    }
    catch ( InvalidPropertiesFormatException e )
    {
      throw new SystemException( "Konfigurationsdatei \"" + filename + "\" ist ungültig.", e );
    }
    catch ( IOException e )
    {
      throw new SystemException( "Konfigurationsdatei \"" + filename + "\" kann nicht gelesen werden.", e );
    }
    catch ( IllegalArgumentException e )
    {
      throw new SystemException( "Konfigurationsdatei \"" + filename + "\" ist ungültig.", e );
    }
    finally
    {
      if ( in != null )
      {
        try
        {
          in.close();
        }
        catch ( IOException e )
        {
          // Exception deliberately ignored.
        }
      }
    }
  }

  private String getPropertyAsString( String key ) throws ConfigurationException
  {
    String value = myproperties.getProperty( key );

    if ( value == null )
    {
      throw new ConfigurationException( "Erforderlicher Konfigurationsparameter \"" + key + "\" ist nicht angegeben." );
    }

    value = value.trim();

    if ( value.length() == 0 )
    {
      throw new ConfigurationException( "Erforderlicher Konfigurationsparameter \"" + key + "\" beinhaltet eine leere Zeichenkette." );
    }

    return value;
  }

  // private void dumpContentIssue( RemoteIssue issue, Vector< RemoteComment > comments )
  // {
  // logger.info( "=========Issue=========" );
  // logger.info( "ID          : " + issue.getId() );
  // logger.info( "Key         : " + issue.getKey() );
  // logger.info( "Type        : " + issue.getType() );
  // logger.info( "Reporter    : " + issue.getReporter() );
  // logger.info( "Project     : " + issue.getProject() );
  // logger.info( "Priority    : " + issue.getPriority() );
  // logger.info( "Status      : " + issue.getStatus() );
  // logger.info( "Enviroment  : " + issue.getEnvironment() );
  // logger.info( "Summary     : " + issue.getSummary() );
  // logger.info( "Description : " + issue.getDescription() );
  // logger.info( "Assign      : " + issue.getAssignee() );
  // logger.info( "Solution    : " + issue.getResolution() );
  // for ( RemoteComment com : comments )
  // {
  // dumpContentComment( com );
  // }
  // }

  private void dumpContentProject( RemoteProject project )
  {
    logger.info( "Project Name:        " + project.getName() );
    logger.info( "Project Key:         " + project.getKey() );
    logger.info( "Project Leader:      " + project.getLead() );
    logger.info( "Project Description: " + project.getDescription() );
  }

  // private void dumpContentComment( RemoteComment comment )
  // {
  // logger.info( "         =========ATTACHMENT=========     " );
  // logger.info( "Author: " + comment.getAuthor() );
  // logger.info( "Text  : " + comment.getBody() );
  // }

  private String issueType( String scarabIssueType ) throws SystemException
  {
    return mappingUtilType.getJiraValueIdFromScarabValue( scarabIssueType );
  }

  private String getPriority( String scarabPrio ) throws SystemException
  {
    return mappingUtilPriority.getJiraValueIdFromScarabValue( scarabPrio );
  }

  private void createJIRADummy( int issueNo, JiraSoapService remote, String token, String projectKey ) throws ConfigurationException, SystemException
  {

    // create a dummy issue in Jira with the default Summary and Description which is set in the
    // property file
    // type is set to "Task"
    // status set on "Close"
    // resolution is set to "Fixed"
    // prio is set to "Trivial"

    try
    {
      String defaultDummyDescription = getPropertyAsString( "defaultDummyDescription" );
      String defaultDummySummary = getPropertyAsString( "defaultDummySummary" );
      String resolution[] = { this.mappingUtilSolution.getJiraValueIdFromScarabValue( "Behoben" ) };

      RemoteFieldValue resolutionField = new RemoteFieldValue( "resolution", resolution );
      RemoteFieldValue[] remoteFields = { resolutionField };

      RemoteIssue dummy = new RemoteIssue();
      dummy.setKey( projectKey + "-" + issueNo );
      dummy.setDescription( defaultDummyDescription );
      dummy.setSummary( defaultDummySummary );
      dummy.setProject( projectKey );
      dummy.setResolution( this.mappingUtilSolution.getJiraValueIdFromScarabValue( "Behoben" ) );
      dummy.setStatus( "6" );
      dummy.setType( this.issueType( "Task" ) );      
      dummy.setPriority( this.getPriority( "Normal" ) );

      remote.createIssue( token, dummy );
      logger.info( "create dummy issue " + dummy.getKey() );
      remote.progressWorkflowAction( token, dummy.getKey(), "2", remoteFields );
      prevNo += 1;

    }
    catch ( ConfigurationException exc )
    {
      throw new ConfigurationException( exc.getMessage(), exc );

    }
    catch ( RemotePermissionException exc )
    {
      throw new SystemException( "login at JIRA server failed, reason: remote error " + exc.getMessage(), exc );
    }
    catch ( RemoteValidationException exc )
    {
      throw new SystemException( exc.getMessage(), exc );
    }
    catch ( RemoteAuthenticationException exc )
    {
      throw new SystemException( "login at JIRA server failed, reason: invalid authentification(username/password)", exc );
    }
    catch ( RemoteException exc )
    {
      throw new SystemException( "login at JIRA server failed, reason: remote error " + exc.getMessage() + " " + exc.getCause(), exc );
    }
    catch ( java.rmi.RemoteException exc )
    {
      throw new SystemException( "login at JIRA server failed, reason: remote error " + exc.getMessage(), exc );
    }
  }

  private void setIndexOfIssues( String key ) throws ConfigurationException
  {
    String scarabprojectname = getPropertyAsString( "scarabprojectticketidentifier" );
    String tmp = key.substring( scarabprojectname.length(), key.length() );
    prevNo = currentNo;
    if ( this.getPropertyAsString( "jiraprojectcreate" ).equalsIgnoreCase( "yes" ) )
    {
      currentNo = Integer.parseInt( tmp );
    } else
    {
      currentNo = Integer.parseInt( tmp ) + startNo;
    }
  }

  private String substringSummary( String summary, RemoteIssue issue )
  {
    String result = summary;
    if ( summary.length() > 255 )
    {
      result = summary.substring( 0, 255 );
      logger.info( "scarab summary truncated to 255 characters to fit in JIRA, JIRA Issue <" + issue.getKey() + ">, scarab summary <" + result
          + ">" );
    }
    return result;
  }

  // extract scarab atributes from previously parsed xml file
  private void extractScarabAttributes( ScarabActivitySet scarabActivitySet, RemoteIssue issue, String scarabIssueId ) throws SystemException, ConfigurationException, IOException
  {
    String change = "";
    String value = "";
    boolean activityWithChange = true;
    boolean activitySetWithChange = false;
    boolean firstActivitySet = false;
    boolean isFirstActivitySet = scarabActivitySet.getType().equalsIgnoreCase( "Create Issue" );

    if ( isFirstActivitySet )
    {
      change = "Erstellung des Vorgangs von " + scarabActivitySet.getEditor() + " am " + scarabActivitySet.getTimestamp() + "\n";
      firstActivitySet = true;
    }
    else
    {
      change = "Änderung des Vorgangs von " + scarabActivitySet.getEditor() + " am " + scarabActivitySet.getTimestamp() + "\n";
      firstActivitySet = false;
    }

    // extract activities
    for ( ScarabActivity scarabActivity : scarabActivitySet.getActivityVector() )
    {
      activityWithChange = true;
      if ( scarabActivity.getAttribute().equalsIgnoreCase( "Kurzbeschreibung" ) )
      {
        issue.setSummary( this.substringSummary( scarabActivity.getNewValue(), issue ) );
        change += "Kurzbeschreibung: " + issue.getSummary() + "\n";
      }
      else if ( scarabActivity.getAttribute().equalsIgnoreCase( "Ausführliche Beschreibung" ) )
      {
        issue.setDescription( this.replaceCriticalSigns( scarabActivity.getNewValue() ) );
        change += "Beschreibung: " + issue.getDescription() + "\n";
      }
      else if ( scarabActivity.getAttribute().equalsIgnoreCase( "Ticket-Typ" ) )
      {
        String scarabType = scarabActivity.getNewValue();
        issue.setType( this.mappingUtilType.getJiraValueIdFromScarabValue( scarabType ) );
        change += "Typ: " + scarabType + "\n";
      }
      else if ( scarabActivity.getAttribute().equalsIgnoreCase( "Priorität" ) )
      {
        String scarabPriority = scarabActivity.getNewValue();
        if ( !scarabPriority.isEmpty() )
        {
          if ( issue.getPriority() != null )
          {
            if ( !issue.getPriority().equals( this.getPriority( scarabPriority ) ) )
            {
              issue.setPriority( this.getPriority( scarabPriority ) );
            }
          }
          else
          {
            issue.setPriority( this.getPriority( scarabPriority ) );
          }
        }
        else
        {
          scarabPriority = "k.A.";
          if ( issue.getPriority() != null )
          {
            if ( !issue.getPriority().equals( this.getPriority( "Normal" ) ) )
            {
              issue.setPriority( this.getPriority( "Normal" ) );
            }
          }
        }
        change += "Priorität: " + scarabPriority + "\n";
      }
      else if ( scarabActivity.getAttribute().equalsIgnoreCase( "Umgebung" ) )
      {
        issue.setEnvironment( scarabActivity.getNewValue() );
        change += "Umgebung: " + issue.getEnvironment() + "\n";
      }
      else if ( scarabActivity.getAttribute().equalsIgnoreCase( "Status" ) )
      {
        issue.setStatus( scarabActivity.getNewValue() );
        change += "Status: " + issue.getStatus() + "\n";
      }
      else if ( scarabActivity.getAttribute().equalsIgnoreCase( "Ergebnis" ) )
      {
        value = scarabActivity.getNewValue();
        issue.setResolution( value );
        if ( value.equalsIgnoreCase( "" ) )
        {
          value = "k.A.";
        }
        change += "Ergebnis: " + value + "\n";
      }
      else
      {
        if ( !firstActivitySet )
        {
          activityWithChange = false;
        }
        for ( String key : additionalScarabAttributes.keySet() )
        {
          if ( scarabActivity.getAttribute().equalsIgnoreCase( key ) )
          {
            if ( key.equalsIgnoreCase( "Zugewiesen" ) )
            {
              if ( !scarabActivity.getNewValue().equalsIgnoreCase( "" ) )
              {
                assignees.add( scarabActivity.getNewValue() );
              }
              else
              {
                String tmp = "";
                for ( String assignee : assignees )
                {
                  tmp = scarabActivity.getOldValue();
                  if ( assignee.equalsIgnoreCase( tmp ) )
                  {
                    break;
                  }
                }
                assignees.remove( tmp );
              }
            }
            else
            {
              value = scarabActivity.getNewValue();
              if ( !value.equals( "" ) )
              {
                additionalScarabAttributes.put( key, value );
              }
              else
              {
                additionalScarabAttributes.put( key, "k.A." );
              }
            }
          }
        }
      }

      if ( activityWithChange )
      {
        activitySetWithChange = true;
      }

      if ( scarabActivity.getDependencyType() != null && !scarabActivity.isDeleted() )
      {
        String parent = scarabActivity.getParent().replaceAll( this.getPropertyAsString( "scarabprojectticketidentifier" ),
            ( this.getPropertyAsString( "jiraprojectkey" ) + "-" ) );
        String child = scarabActivity.getChild().replaceAll( this.getPropertyAsString( "scarabprojectticketidentifier" ),
            ( this.getPropertyAsString( "jiraprojectkey" ) + "-" ) );
        if ( scarabActivity.getDependencyType().equalsIgnoreCase( "duplicate" ) )
        {
          dependencies.add( "Der Vorgang " + child + " ist ein Duplikat von " + parent );
        }
        else if ( scarabActivity.getDependencyType().equalsIgnoreCase( "non-blocking" ) )
        {
          dependencies.add( "Der Vorgang " + child + " ist assoziert mit " + parent );
        }
        else if ( scarabActivity.getDependencyType().equalsIgnoreCase( "blocking" ) )
        {
          dependencies.add( "Der Vorgang " + parent + " ist Voraussetzung für " + child );
        }
      }

      if ( scarabActivity.getAttachmentType().equalsIgnoreCase( "ATTACHMENT" ) && !scarabActivity.isDeleted() )
      {
        File attachment = this.attachmentLoader.getAttachment( scarabIssueId, scarabActivity.getAttachmentId(),
            scarabActivity.getAttachmentFilename() );
        if ( attachment != null )
        {
          // only import the attachment if it is < 50MByte to avoid out of memory exception on jira
          // server
          if ( !( attachment.length() > 41943040L ) )
          {
            attachmentFilenames.add( scarabActivity.getAttachmentFilename() );
            attachmentData.add( Base64Encoder.encodeFile( attachment ) );
          }
          else
          {
            logger.info( "Bei Issue <" + issue.getId() + "> konnte der Anhang <" + scarabActivity.getAttachmentFilename()
                + "> nicht migriert werden" );
            notImportedAttachments.add( "Der Anhang " + scarabActivity.getAttachmentFilename()
                + " wurde aufgrund seiner Größe nicht automatisch migriert.\n" );
          }
        }
      }

      if ( scarabActivity.getAttachmentType().equalsIgnoreCase( "COMMENT" ) )
      {
        String commentBody = this.replaceCriticalSigns( scarabActivity.getCommentBody() );

        if ( !commentBody.isEmpty() )
        {
          commentBody = "Kommentar von " + scarabActivity.getCommentName() + " am " + scarabActivitySet.getTimestamp() + "\n" + commentBody
              + "\n";
          comments.add( commentBody );
        }
      }

      change += "\n";
    }

    if ( activitySetWithChange )
    {
      changes.add( change );
      modificationComments.add( replaceCriticalSigns( scarabActivitySet.getComment() ) );
    }

  }

  private boolean createAndUpdateJiraIssue( RemoteIssue issue, String token, JiraSoapService remote, String scarabId ) throws java.rmi.RemoteException, SystemException, ConfigurationException
  {
    String scarabSolution = issue.getResolution();
    String scarabStatus = issue.getStatus();
    String priority = issue.getPriority();
    String summary = issue.getSummary();
    String resolution[] = new String[ 1 ];

    RemoteFieldValue resolutionField = new RemoteFieldValue( "resolution", resolution );
    RemoteFieldValue[] remoteFields = { resolutionField };

    if ( scarabStatus == null )
    {
      this.createJIRADummy( prevNo + 1, remote, token, getPropertyAsString( "jiraprojectkey" ) );
      logger.debug( "scarab issue no. <" + currentNo + "> has no status; dummy will be created" );

      return false;
    }

    if ( scarabSolution != null && !scarabSolution.equalsIgnoreCase( "" ) )
    {
      resolution[ 0 ] = this.mappingUtilSolution.getJiraValueIdFromScarabValue( scarabSolution );
    }

    if ( priority == null )
    {
      issue.setPriority( this.getPriority( "Normal" ) );
    }

    if ( summary == null )
    {
      issue.setSummary( "k.A." );
    }

    remote.createIssue( token, issue );
    logger.info( "create  issue " + issue.getKey() + " scarab id " + scarabId );

    // adding the attachments
    Vector[] tmpAttachmentVectors = attachments.get( scarabId );
    if ( tmpAttachmentVectors != null )
    {
      String[] tmpAttachmentFilenames = new String[ tmpAttachmentVectors[ 0 ].size() ];
      String[] tmpAttachmentData = new String[ tmpAttachmentVectors[ 1 ].size() ];
      attachmentVectors[ 0 ].toArray( tmpAttachmentFilenames );
      attachmentVectors[ 1 ].toArray( tmpAttachmentData );
      remote.addBase64EncodedAttachmentsToIssue( token, issue.getKey(), tmpAttachmentFilenames, tmpAttachmentData );
    }

    // updating jira status and solution
    if ( scarabStatus.equalsIgnoreCase( "Geschlossen" ) )
    {
      if ( resolution[ 0 ] == null )
      {
        resolution[ 0 ] = this.mappingUtilSolution.getJiraValueIdFromScarabValue( "Behoben" );
      }
      remote.progressWorkflowAction( token, issue.getKey(), "2", remoteFields );
      logger.info( "updated issue " + issue.getKey() + ": new status is Closed" );
    }
    else if ( scarabStatus.equalsIgnoreCase( "Wiedereroeffnet" ) )
    {
      resolution[ 0 ] = this.mappingUtilSolution.getJiraValueIdFromScarabValue( "Behoben" );
      remote.progressWorkflowAction( token, issue.getKey(), "2", remoteFields );
      remote.progressWorkflowAction( token, issue.getKey(), "3", remoteFields );
      logger.info( "updated issue " + issue.getKey() + ": new status is Reopened" );
    }
    else if ( scarabStatus.equalsIgnoreCase( "Gefixt" ) )
    {
      if ( resolution[ 0 ] == null )
      {
        resolution[ 0 ] = this.mappingUtilSolution.getJiraValueIdFromScarabValue( "Behoben" );
      }
      remote.progressWorkflowAction( token, issue.getKey(), "5", remoteFields );
      logger.info( "updated issue " + issue.getKey() + ": new status is Resolved" );
    }
    else
    {
      // if scarab status is 'Neu' do nothing, Jira status is already open
      if ( !( scarabStatus.equalsIgnoreCase( "Neu" ) || scarabStatus.equalsIgnoreCase( "Zugewiesen" )
          || scarabStatus.equalsIgnoreCase( "Warten extern" ) || scarabStatus.equalsIgnoreCase( "Test Extern" ) ) )
      {
        throw new SystemException( "Nicht berücksichtigter Scarab Status <" + scarabStatus + ">" );
      }
    }
    return true;
  }

  private void addCommentWithChangeHistory( String token, RemoteIssue issue, JiraSoapService remote ) throws java.rmi.RemoteException
  {
    RemoteComment comment = new RemoteComment();
    String commentBody = "";
    StringBuffer buf = new StringBuffer();

    for ( int index = 0; index < changes.size(); index++ )
    {
      if ( index == 0 )
      {
        buf.append( "*Änderungshistorie*\n\n" );
      }
      buf.append( ( index + 1 ) + ". " + changes.get( index ) + "\n\n" );

      if ( modificationComments.get( index ) != null )
      {
        buf.append( "Änderungskommentar: " + modificationComments.get( index ) + "\n" );
      }
      buf.append( "\n" );
    }

    if ( dependencies.size() > 0 )
    {
      buf.append( "*Es bestehen folgende Relationen*\n\n" );
      for ( String dependency : dependencies )
      {
        buf.append( dependency + "\n" );
      }
      buf.append( "\n" );
    }

    if ( additionalScarabAttributes.size() > 0 )
    {
      buf.append( "*Werte zusätzlicher Scarab Attribute*\n\n" );
      for ( String key : additionalScarabAttributes.keySet() )
      {
        if ( !additionalScarabAttributes.get( key ).equalsIgnoreCase( "k.A." ) )
        {
          buf.append( key + ": " + additionalScarabAttributes.get( key ) + "\n" );
        }
      }
      buf.append( "\n" );
    }

    if ( comments.size() > 0 )
    {
      buf.append( "*Kommentare*\n\n" );
      for ( String com : comments )
      {
        buf.append( com + "\n" );
      }
      buf.append( "\n" );
    }

    if ( notImportedAttachments.size() > 0 )
    {
      buf.append( "*Hinweis*\n\n" );
      for ( String notImportedAttachment : notImportedAttachments )
      {
        buf.append( notImportedAttachment + "\n" );
      }
    }

    commentBody = buf.toString();

    if ( !commentBody.isEmpty() )
    {
      comment.setBody( commentBody );
      remote.addComment( token, issue.getKey(), comment );
    }
  }

  private String replaceCriticalSigns( String string )
  {
    String tmp = string;
    if ( tmp != null )
    {
      tmp = tmp.trim();     
      
      // Jira SQL DB Encoding-Problematik
      // if jira db is not running with utf-8 there can be errors on scarab2jira import (and later copy & pase usage of jira)
      // temporary solution: uncomment the next line               
      tmp = tmp.replaceAll( "[^\\p{InBasic_Latin}\\p{InLatin_1_Supplement}]", " " );                  
    }
    return tmp;
  }

}
